﻿using MyAirport.Pim.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace MyAirport.Pim.Model
{
    public class Sql : AbstractDefinition
    {
        private bool use                    = false;
        private string userConnectionString = string.Empty;
        private string strcnx               = ConfigurationManager.ConnectionStrings["MyAirport.Pim.Settings.DbConnect"].ConnectionString;


        string commandGetBagage =
                "SELECT b.ID_BAGAGE,  b.COMPAGNIE, b.LIGNE, b.JOUR_EXPLOITATION, b.ESCALE, b.CLASSE, b.CODE_IATA, b.CONTINUATION, "+
                "iif(p.PARTICULARITE is null, cast(0 as BIT), cast(1 as Bit)) as RUSH "+
                "FROM BAGAGE b "+
                "LEFT OUTER JOIN BAGAGE_A_POUR_PARTICULARITE app ON app.ID_BAGAGE = b.ID_BAGAGE and app.ID_PARTICULARITE = 15 " +
                "LEFT OUTER JOIN BAGAGE_PARTICULARITE p on p.ID_PART = app.ID_PARTICULARITE " +
                "WHERE b.CODE_IATA = @code "+
                "ORDER BY ID_BAGAGE desc";

        string commandCreateBagage =
                "INSERT INTO BAGAGE (CODE_IATA, COMPAGNIE, LIGNE, JOUR_EXPLOITATION, ESCALE, CLASSE, CONTINUATION, ORIGINE_CREATION, DATE_CREATION) " +
                "OUTPUT INSERTED.ID_BAGAGE "+
                "VALUES (@code, @compagnie, @ligne, @jour, @escale, @classe, @continuation, 'O', CURRENT_TIMESTAMP)";

        string commandAddRush =
                "INSERT INTO BAGAGE_A_POUR_PARTICULARITE (ID_BAGAGE, ID_PARTICULARITE)" +
                "VALUES (@bagage, 15)";

        public Sql()
        {
            this.setConnection();
        }

        public override List<BagageDefinition> GetBagage(int idBagage)
        {
            throw new NotImplementedException();
        }

        public override List<BagageDefinition> GetBagage(string codeIata)
        {
            string connectionString = this.use ? this.userConnectionString : strcnx;
            using (SqlConnection cnx = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(commandGetBagage, cnx);
                cmd.Parameters.AddWithValue("@code", codeIata);
                cnx.Open();

                List<BagageDefinition> list = null;
             
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    BagageDefinition bag = null;
                    // Au moins un bagage à été trouvé
                    while (sdr.Read())
                    {
                        if (list == null)
                        { list = new List<BagageDefinition>(); }
                        bag = new BagageDefinition();
                        #region cache
                        bag.IdBagage = sdr.GetInt32(0);//Attention ici à l'ordre des colonnes retrounées par la requête
                        bag.Compagnie = sdr["COMPAGNIE"].ToString();
                        bag.Ligne = Convert.ToInt32(sdr["LIGNE"]);
                        bag.JourExploitation = sdr.GetInt16(sdr.GetOrdinal("JOUR_EXPLOITATION")); //Attention le type demander doit correspondre au type SQL Getint32 léve une exception
                        bag.Itineraire = sdr.GetString(sdr.GetOrdinal("ESCALE"));
                        bag.ClasseBagage = sdr["classe"] is DBNull ? 'Y' : Convert.ToChar(sdr["CLASSE"]);
                        bag.CodeIata = sdr.GetString(sdr.GetOrdinal("CODE_IATA"));
                        bag.Continuation = sdr[sdr.GetOrdinal("CONTINUATION")].ToString() == "Y" ? true : false; //et pas 'N' ? false : true; car sinon on considére le passager en continuation par defaut
                        bag.Rush = sdr.GetBoolean(sdr.GetOrdinal("RUSH"));
                        #endregion
                        list.Add(bag);
                    }
                    
                    return list;
                }
            }
        }
        public override int CreateBagage(Entities.BagageDefinition bag)
        {
            string connectionString = this.use ? this.userConnectionString : strcnx;
            using (SqlConnection cnx = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(commandCreateBagage, cnx);
                cmd.Parameters.AddWithValue("@code", bag.CodeIata);
                cmd.Parameters.AddWithValue("@compagnie", bag.Compagnie);
                cmd.Parameters.AddWithValue("@ligne", bag.Ligne);
                cmd.Parameters.AddWithValue("@jour", bag.JourExploitation);
                cmd.Parameters.AddWithValue("@escale", bag.Itineraire);
                cmd.Parameters.AddWithValue("@classe", bag.ClasseBagage.Equals("") ? 'Y' : bag.ClasseBagage);
                cmd.Parameters.AddWithValue("@continuation", bag.Continuation == true ? "Y" : "N");
                cnx.Open();

                int newId = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                if (bag.Rush == true)
                {
                    SqlCommand rush = new SqlCommand(commandAddRush, cnx);
                    rush.Parameters.AddWithValue("@bagage", newId);
                    rush.ExecuteNonQuery();
                }
   
                return 42;
            }
        }

        public override Entities.RoutageBagage GetInfoRoutage(int idBagage)
        {
            return new Entities.RoutageBagage()
            {
                LocalisationEjection = 4516,
                NomEjection = "EJ13",
                StatutEjection = "TPS",
                LocalisationSureteN1 = 4385,
                NomSureteN1 = "PIM EST",
                LocalisationSureteN3 = 13284,
                NomSureteN3 = "CX1"
            };
        }

        public override void setConnection()
        {
            Configuration conf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            ConfigurationSectionGroup group = conf.GetSectionGroup("userSettings");
            ClientSettingsSection section = group.Sections["Server.Host.Properties.Settings"] as ClientSettingsSection;
            foreach (SettingElement setting in section.Settings)
            {
                Console.WriteLine(setting.Name + " : " + setting.Value.ValueXml.InnerText);
                switch (setting.Name)
                {
                    case "Test":
                        this.userConnectionString = setting.Value.ValueXml.InnerText;
                        break;
                    case "UseConnection":
                        this.use = Convert.ToBoolean(setting.Value.ValueXml.InnerText); ;
                        break;
                    default:
                        break;
                }
            }
            //Console.WriteLine("SQL set connection");
            //Console.WriteLine("Connection string : " + this.userConnectionString);
            //Console.WriteLine("Use user connection : " + this.use);
        }
    }
}
