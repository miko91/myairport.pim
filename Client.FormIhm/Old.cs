﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client.FormIhm
{
    public partial class Old : Form
    {
        ServiceBagageReference.ServiceBagageClient proxy = null;

        public Old()
        {
            InitializeComponent();
            this.comboBox1.SelectedIndex = 0;
            this.button2.Hide();
            proxy = new ServiceBagageReference.ServiceBagageClient();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.button2.Hide();
            string code = this.comboBox1.Text;
            try
            {
                var bagage = proxy.GetBagageByCodeIata(code);

                #region LoadBagage
                this.tbAlpha.Text = bagage.LigneAlpha.ToString();
                this.tbClasseBag.Text = bagage.ClasseBagage.ToString();
                this.tbCodeIata.Text = bagage.CodeIata;
                this.tbCompagnie.Text = bagage.Compagnie;
                this.tbItineraire.Text = bagage.Itineraire;
                this.tbJourExploitation.Text = bagage.JourExploitation.ToString();
                this.tbLigne.Text = bagage.Ligne.ToString();
                this.cbContinuation.Checked = bagage.Continuation;
                this.cbRush.Checked = bagage.Rush;
                #endregion
            }
            catch (FaultException<ServiceBagageReference.MultipleBagageFault> excp)
            {
                List<string> detail = new List<string>();
                foreach (ServiceBagageReference.BagageDefinition b in excp.Detail.ListBagages) { detail.Add(b.IdBagage.ToString()); }
                this.log(excp.Detail.Message, detail);
            }
            catch (CommunicationException excp)
            {
                this.log(
                    "Erreur de communication",
                    new List<string>()
                    {
                        "Type: " + excp.GetType().ToString(),
                        "Message: " + excp.Message
                    }
                );
            }
            catch (NullReferenceException nexcp)
            {
                this.log(
                    "Aucun bagage avec le code IATA : " + code,
                    new List<string>()
                    {
                        "Vous pouvez le créer en remplissant les champs puis en cliquant sur Add"
                    }
                );
                this.button2.Show();
            }
            catch (Exception excp)
            {
                this.log(
                    "Erreur de traitement",
                    new List<string>()
                    {
                        excp.GetType().ToString(), excp.Message
                    }
                );
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("You want to add a bagage");
            ServiceBagageReference.BagageDefinition bagage = new ServiceBagageReference.BagageDefinition();

            #region CreateBagage
            bagage.CodeIata         = this.tbCodeIata.Text;
            bagage.Compagnie        = this.tbCompagnie.Text;
            bagage.Itineraire       = this.tbItineraire.Text;
            bagage.Continuation     = this.cbContinuation.Checked;
            bagage.Rush             = this.cbRush.Checked;
            if (this.tbClasseBag.Text.Count() == 1)
            { bagage.ClasseBagage = Convert.ToChar(this.tbClasseBag.Text); }
            if (this.tbJourExploitation.Text.Count() == 1)
            { bagage.JourExploitation = Convert.ToInt32(this.tbJourExploitation.Text); }
            if (this.tbLigne.Text.Count() > 0)
            { bagage.Ligne = Convert.ToInt32(this.tbLigne.Text); }
            #endregion

            try
            {
                int create = proxy.CreateBagage(bagage);
                this.button2.Hide();
                this.log("Ajout de bagage avec le code (" + bagage.CodeIata + ")", new List<string>() { });
            }
            catch (FaultException<ServiceBagageReference.AddBagageFault> exp)
            {
                this.log(
                    "Erreur d'ajout",
                    new List<string>()
                    {
                       exp.Detail.Message
                    }
                );
            }
        }

        private void log(string message, List<string> detail)
        {
            string time = DateTime.Now.ToString("dd/MM/yyyy H:mm:ss");
            this.listBox1.Items.Add(time + "\t" + message);
            foreach (string item in detail)
            {
                this.listBox1.Items.Add("\t\t\t" + item);
            }
            this.listBox1.SetSelected(this.listBox1.Items.Count - 1, true);
            this.listBox1.SetSelected(this.listBox1.Items.Count - 1, false);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddForm child = new AddForm();
            child.MdiParent = this;
            child.Visible = true;
            child.Show();
            MessageBox.Show("Test child");
        }
    }
}
