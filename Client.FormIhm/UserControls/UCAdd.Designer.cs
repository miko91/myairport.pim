﻿namespace Client.FormIhm.UserControls
{
    partial class UCAdd
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.addButton = new System.Windows.Forms.Button();
            this.codeBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rushCheck = new System.Windows.Forms.CheckBox();
            this.alphaBox = new System.Windows.Forms.TextBox();
            this.ligneBox = new System.Windows.Forms.TextBox();
            this.jourBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.escaleBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.continuationCheck = new System.Windows.Forms.CheckBox();
            this.classeBox = new System.Windows.Forms.TextBox();
            this.compagnieBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.addButton);
            this.groupBox1.Controls.Add(this.codeBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.rushCheck);
            this.groupBox1.Controls.Add(this.alphaBox);
            this.groupBox1.Controls.Add(this.ligneBox);
            this.groupBox1.Controls.Add(this.jourBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.escaleBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.continuationCheck);
            this.groupBox1.Controls.Add(this.classeBox);
            this.groupBox1.Controls.Add(this.compagnieBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(10, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 255);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ajouter";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(132, 20);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(100, 23);
            this.addButton.TabIndex = 32;
            this.addButton.Text = "Ajouter\r\nun bagage";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // codeBox
            // 
            this.codeBox.Location = new System.Drawing.Point(132, 55);
            this.codeBox.Name = "codeBox";
            this.codeBox.Size = new System.Drawing.Size(100, 20);
            this.codeBox.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "Code IATA";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Rush";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Ligne / Aplha";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Jour exploitation";
            // 
            // rushCheck
            // 
            this.rushCheck.AutoSize = true;
            this.rushCheck.Location = new System.Drawing.Point(132, 237);
            this.rushCheck.Name = "rushCheck";
            this.rushCheck.Size = new System.Drawing.Size(15, 14);
            this.rushCheck.TabIndex = 26;
            this.rushCheck.UseVisualStyleBackColor = true;
            // 
            // alphaBox
            // 
            this.alphaBox.Location = new System.Drawing.Point(187, 210);
            this.alphaBox.Name = "alphaBox";
            this.alphaBox.Size = new System.Drawing.Size(45, 20);
            this.alphaBox.TabIndex = 25;
            // 
            // ligneBox
            // 
            this.ligneBox.Location = new System.Drawing.Point(132, 210);
            this.ligneBox.Name = "ligneBox";
            this.ligneBox.Size = new System.Drawing.Size(48, 20);
            this.ligneBox.TabIndex = 24;
            // 
            // jourBox
            // 
            this.jourBox.Location = new System.Drawing.Point(132, 183);
            this.jourBox.Name = "jourBox";
            this.jourBox.Size = new System.Drawing.Size(100, 20);
            this.jourBox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Itinéraire";
            // 
            // escaleBox
            // 
            this.escaleBox.Location = new System.Drawing.Point(132, 156);
            this.escaleBox.Name = "escaleBox";
            this.escaleBox.Size = new System.Drawing.Size(100, 20);
            this.escaleBox.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Continuation";
            // 
            // continuationCheck
            // 
            this.continuationCheck.AutoSize = true;
            this.continuationCheck.Location = new System.Drawing.Point(132, 135);
            this.continuationCheck.Name = "continuationCheck";
            this.continuationCheck.Size = new System.Drawing.Size(15, 14);
            this.continuationCheck.TabIndex = 19;
            this.continuationCheck.UseVisualStyleBackColor = true;
            // 
            // classeBox
            // 
            this.classeBox.Location = new System.Drawing.Point(132, 81);
            this.classeBox.Name = "classeBox";
            this.classeBox.Size = new System.Drawing.Size(100, 20);
            this.classeBox.TabIndex = 18;
            // 
            // compagnieBox
            // 
            this.compagnieBox.Location = new System.Drawing.Point(132, 108);
            this.compagnieBox.Name = "compagnieBox";
            this.compagnieBox.Size = new System.Drawing.Size(100, 20);
            this.compagnieBox.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Compagnie";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Classe bagage";
            // 
            // UCAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCAdd";
            this.Size = new System.Drawing.Size(415, 290);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox rushCheck;
        private System.Windows.Forms.TextBox alphaBox;
        private System.Windows.Forms.TextBox jourBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox escaleBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox continuationCheck;
        private System.Windows.Forms.TextBox classeBox;
        private System.Windows.Forms.TextBox compagnieBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ligneBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox codeBox;
        private System.Windows.Forms.Button addButton;
    }
}
