﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client.FormIhm.UserControls
{
    public partial class AbstractUC : UserControl
    {
        protected Main controller;

        public AbstractUC()
        {
            InitializeComponent();
        }

        public AbstractUC(Main controller)
        {
            this.controller = controller;
            InitializeComponent();
        }

        virtual public void clearUC() { }
    }
}
