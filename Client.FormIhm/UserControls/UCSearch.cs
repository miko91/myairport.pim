﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;

namespace Client.FormIhm.UserControls
{
    public partial class UCSearch : AbstractUC
    {
        private Dictionary<int, ServiceBagageReference.BagageDefinition> bagList = null;

        public UCSearch()
        {
            InitializeComponent();
        }

        public UCSearch(Main controller) : base(controller)
        {
            InitializeComponent();
        }

        override public void clearUC()
        {
            //this.searchBox.Text             = string.Empty;
            this.classeBox.Text             = string.Empty;
            this.compagnieBox.Text          = string.Empty;
            this.continuationCheck.Checked  = false;
            this.escaleBox.Text             = string.Empty;
            this.jourBox.Text               = string.Empty;
            this.ligneBox.Text              = string.Empty;
            this.alphaBox.Text              = string.Empty;
            this.rushCheck.Checked          = false;
            this.multipleList.Items.Clear();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            this.clearUC();
            string code = this.searchBox.Text;
            try
            {
                var bagage = this.controller.getProxy().GetBagageByCodeIata(code);
                this.displayBagage(bagage);
            }
            catch (FaultException<ServiceBagageReference.MultipleBagageFault> excp)
            {
                this.controller.log(excp.Detail.Message, new List<string>());
                this.setBagList(excp.Detail.ListBagages.ToList());
            }
            catch (CommunicationException excp)
            {
                this.controller.log(
                    "Erreur de communication",
                    new List<string>()
                    {
                        "Type: " + excp.GetType().ToString(),
                        "Message: " + excp.Message
                    }
                );
            }
            catch (NullReferenceException nexcp)
            {
                this.controller.log(
                    "Aucun bagage avec le code IATA : " + code,
                    new List<string>()
                    {
                        nexcp.Message,
                        "Vous pouvez le créer depuis Fichier -> Ajouter bagage"
                    }
                );
            }
            catch (Exception excp)
            {
                this.controller.log(
                    "Erreur de traitement",
                    new List<string>()
                    {
                        excp.GetType().ToString(), excp.Message
                    }
                );
            }
        }

        private void setBagList(List<ServiceBagageReference.BagageDefinition> list)
        {
            this.multipleList.Items.Clear();
            this.bagList = new Dictionary<int, ServiceBagageReference.BagageDefinition>();
            foreach (ServiceBagageReference.BagageDefinition bag in list)
            {
                this.bagList.Add(bag.IdBagage, bag);
                this.multipleList.Items.Add(bag.IdBagage);
            }
        }

        private void displayBagage(ServiceBagageReference.BagageDefinition bagage)
        {
            this.alphaBox.Text              = bagage.LigneAlpha.ToString();
            this.classeBox.Text             = bagage.ClasseBagage.ToString();
            this.compagnieBox.Text          = bagage.Compagnie;
            this.escaleBox.Text             = bagage.Itineraire;
            this.jourBox.Text               = bagage.JourExploitation.ToString();
            this.ligneBox.Text              = bagage.Ligne.ToString();
            this.continuationCheck.Checked  = bagage.Continuation;
            this.rushCheck.Checked          = bagage.Rush;
        }

        private void multipleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var bagage = this.bagList[Convert.ToInt32(this.multipleList.SelectedItem.ToString())];
                this.displayBagage(bagage);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "MyAirport Client - Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void searchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.searchButton_Click(sender, e);
            }
        }
    }

}
