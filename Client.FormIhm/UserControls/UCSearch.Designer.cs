﻿namespace Client.FormIhm.UserControls
{
    partial class UCSearch
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.multipleList = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rushCheck = new System.Windows.Forms.CheckBox();
            this.alphaBox = new System.Windows.Forms.TextBox();
            this.ligneBox = new System.Windows.Forms.TextBox();
            this.jourBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.escaleBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.continuationCheck = new System.Windows.Forms.CheckBox();
            this.classeBox = new System.Windows.Forms.TextBox();
            this.compagnieBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.searchButton);
            this.groupBox1.Controls.Add(this.searchBox);
            this.groupBox1.Location = new System.Drawing.Point(10, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 55);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Code IATA";
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(132, 20);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(100, 23);
            this.searchButton.TabIndex = 1;
            this.searchButton.Text = "Rechercher";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // searchBox
            // 
            this.searchBox.Location = new System.Drawing.Point(7, 20);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(100, 20);
            this.searchBox.TabIndex = 0;
            this.searchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchBox_KeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.multipleList);
            this.groupBox2.Location = new System.Drawing.Point(260, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(150, 255);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bagages multiples";
            // 
            // multipleList
            // 
            this.multipleList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.multipleList.FormattingEnabled = true;
            this.multipleList.Location = new System.Drawing.Point(6, 20);
            this.multipleList.Name = "multipleList";
            this.multipleList.Size = new System.Drawing.Size(138, 221);
            this.multipleList.TabIndex = 0;
            this.multipleList.SelectedIndexChanged += new System.EventHandler(this.multipleList_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.rushCheck);
            this.groupBox3.Controls.Add(this.alphaBox);
            this.groupBox3.Controls.Add(this.ligneBox);
            this.groupBox3.Controls.Add(this.jourBox);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.escaleBox);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.continuationCheck);
            this.groupBox3.Controls.Add(this.classeBox);
            this.groupBox3.Controls.Add(this.compagnieBox);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(10, 91);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(243, 194);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Information bagage";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Rush";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Ligne / Aplha";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Jour exploitation";
            // 
            // rushCheck
            // 
            this.rushCheck.AutoCheck = false;
            this.rushCheck.AutoSize = true;
            this.rushCheck.Location = new System.Drawing.Point(132, 176);
            this.rushCheck.Name = "rushCheck";
            this.rushCheck.Size = new System.Drawing.Size(15, 14);
            this.rushCheck.TabIndex = 11;
            this.rushCheck.UseVisualStyleBackColor = true;
            // 
            // alphaBox
            // 
            this.alphaBox.Location = new System.Drawing.Point(187, 149);
            this.alphaBox.Name = "alphaBox";
            this.alphaBox.ReadOnly = true;
            this.alphaBox.Size = new System.Drawing.Size(45, 20);
            this.alphaBox.TabIndex = 10;
            // 
            // ligneBox
            // 
            this.ligneBox.Location = new System.Drawing.Point(132, 149);
            this.ligneBox.Name = "ligneBox";
            this.ligneBox.ReadOnly = true;
            this.ligneBox.Size = new System.Drawing.Size(48, 20);
            this.ligneBox.TabIndex = 9;
            // 
            // jourBox
            // 
            this.jourBox.Location = new System.Drawing.Point(132, 122);
            this.jourBox.Name = "jourBox";
            this.jourBox.ReadOnly = true;
            this.jourBox.Size = new System.Drawing.Size(100, 20);
            this.jourBox.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Itinéraire";
            // 
            // escaleBox
            // 
            this.escaleBox.Location = new System.Drawing.Point(132, 95);
            this.escaleBox.Name = "escaleBox";
            this.escaleBox.ReadOnly = true;
            this.escaleBox.Size = new System.Drawing.Size(100, 20);
            this.escaleBox.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Continuation";
            // 
            // continuationCheck
            // 
            this.continuationCheck.AutoCheck = false;
            this.continuationCheck.AutoSize = true;
            this.continuationCheck.Location = new System.Drawing.Point(132, 74);
            this.continuationCheck.Name = "continuationCheck";
            this.continuationCheck.Size = new System.Drawing.Size(15, 14);
            this.continuationCheck.TabIndex = 4;
            this.continuationCheck.UseVisualStyleBackColor = true;
            // 
            // classeBox
            // 
            this.classeBox.Location = new System.Drawing.Point(132, 20);
            this.classeBox.Name = "classeBox";
            this.classeBox.ReadOnly = true;
            this.classeBox.Size = new System.Drawing.Size(100, 20);
            this.classeBox.TabIndex = 3;
            // 
            // compagnieBox
            // 
            this.compagnieBox.Location = new System.Drawing.Point(132, 47);
            this.compagnieBox.Name = "compagnieBox";
            this.compagnieBox.ReadOnly = true;
            this.compagnieBox.Size = new System.Drawing.Size(100, 20);
            this.compagnieBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Compagnie";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Classe bagage";
            // 
            // UCSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "UCSearch";
            this.Size = new System.Drawing.Size(415, 290);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox classeBox;
        private System.Windows.Forms.TextBox compagnieBox;
        private System.Windows.Forms.CheckBox continuationCheck;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox escaleBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox jourBox;
        private System.Windows.Forms.TextBox alphaBox;
        private System.Windows.Forms.TextBox ligneBox;
        private System.Windows.Forms.CheckBox rushCheck;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox multipleList;
    }
}
