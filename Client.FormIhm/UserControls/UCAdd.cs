﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;

namespace Client.FormIhm.UserControls
{
    public partial class UCAdd : AbstractUC
    {
        public UCAdd()
        {
            InitializeComponent();
        }

        public UCAdd(Main controller) : base(controller)
        {
            InitializeComponent();
        }

        public override void clearUC()
        {
            
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            ServiceBagageReference.BagageDefinition bagage = new ServiceBagageReference.BagageDefinition();

            #region CreateBagage
            bagage.CodeIata     = this.codeBox.Text;
            bagage.Compagnie    = this.compagnieBox.Text;
            bagage.Itineraire   = this.escaleBox.Text;
            bagage.Continuation = this.continuationCheck.Checked;
            bagage.Rush         = this.rushCheck.Checked;

            if (this.classeBox.Text.Count() == 1)
            { bagage.ClasseBagage = Convert.ToChar(this.classeBox.Text); }
            if (this.jourBox.Text.Count() == 1)
            { bagage.JourExploitation = Convert.ToInt32(this.jourBox.Text); }
            if (this.ligneBox.Text.Count() > 0)
            { bagage.Ligne = Convert.ToInt32(this.ligneBox.Text); }
            #endregion

            try
            {
                int create = this.controller.getProxy().CreateBagage(bagage);
                this.controller.log("Ajout de bagage avec le code \"" + bagage.CodeIata + "\"", new List<string>() { });
            }
            catch (FaultException<ServiceBagageReference.AddBagageFault> exp)
            {
                this.controller.log(
                    "Erreur d'ajout",
                    new List<string>()
                    {
                       exp.Detail.Message
                    }
                );
            }
            catch (Exception otherX)
            {
                this.controller.log(
                    "Erreur",
                    new List<string>()
                    {
                        "Type: " + otherX.GetType().ToString(),
                        "Message: " + otherX.Message
                    }
                );
            }
        }
    }
}
