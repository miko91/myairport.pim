﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Client.FormIhm
{
    public partial class Main : Form
    {

        private UserControls.AbstractUC cSearch = null;
        private UserControls.AbstractUC cAdd    = null;
        private ServiceBagageReference.ServiceBagageClient proxy = null;

        public Main()
        {
            InitializeComponent();
            // Add user controls
            this.cSearch = new UserControls.UCSearch(this);
            this.Controls.Add(cSearch);
            this.cAdd = new UserControls.UCAdd(this);
            this.Controls.Add(cAdd);
            // Display firt user control (search view)
            this.displayUserControl(this.cSearch);
            // Create the proxy
            this.proxy = new ServiceBagageReference.ServiceBagageClient();
            this.log(
                "Creation du proxy",
                new List<string>()
            );
        }

        public ServiceBagageReference.ServiceBagageClient getProxy()
        {
            return this.proxy;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            displayUserControl(this.cSearch);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Voulez-vous quitter l'application ?", "MyAirport Client", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            { System.Windows.Forms.Application.Exit(); }
        }

        private void addBagageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            displayUserControl(this.cAdd);
        }

        private void displayUserControl(UserControls.AbstractUC uControl)
        {
            string controlName = string.Empty;
            foreach (Control control in this.Controls)
            {
                controlName = control.Name;
                if (controlName.StartsWith("UC"))
                {
                    if (control == uControl)
                    {
                        uControl.clearUC();
                        control.Dock = DockStyle.Fill;
                        control.Enabled = true;
                        control.Visible = true;
                        //control.BringToFront();
                    }
                    else
                    {
                        control.Visible = false;
                    }
                }
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new UserControls.AboutBox1()).Show();
        }

        private void documentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://myairport.mika-p.fr");
        }

        public void log(string message, List<string> detail)
        {
            string time = DateTime.Now.ToString("dd/MM/yyyy H:mm:ss");
            this.logList.Items.Add(time + "\t" + message);
            foreach (string item in detail)
            {
                this.logList.Items.Add("\t\t\t" + item);
            }
            this.logList.SetSelected(this.logList.Items.Count - 1, true);
            this.logList.SetSelected(this.logList.Items.Count - 1, false);
        }
    }
}
