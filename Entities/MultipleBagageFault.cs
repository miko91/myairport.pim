﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace MyAirport.Pim.Entities
{
    [DataContract]
    sealed public class MultipleBagageFault
    {
        [DataMember]
        public List<BagageDefinition> ListBagages { get; set; }
        [DataMember]
        public String CodeIata { get; set; }
        [DataMember]
        public String Message { get; set; }
    }
}
