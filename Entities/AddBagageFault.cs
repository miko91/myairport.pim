﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace MyAirport.Pim.Entities
{
    [DataContract]
    sealed public class AddBagageFault
    {
        [DataMember]
        public string Message { get; set; }
    }
}
