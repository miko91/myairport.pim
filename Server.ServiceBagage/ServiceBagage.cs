﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using MyAirport.Pim.Entities;

namespace Server.ServiceBagage
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "Service1" à la fois dans le code et le fichier de configuration.
    public class ServiceBagage : IServiceBagage
    {
        public static void setConnetion()
        {
            MyAirport.Pim.Model.Factory.Model.setConnection();
        }

        public int CreateBagage(BagageDefinition bag)
        {
            System.Diagnostics.Debug.WriteLine("Call CreateBagage");
            try
            {
                return MyAirport.Pim.Model.Factory.Model.CreateBagage(bag);
            }
            catch (System.Data.SqlClient.SqlException sql)
            {
                throw new FaultException<AddBagageFault>(new AddBagageFault()
                {
                    Message = sql.Message
                });
            }
            catch (Exception excp)
            {
                throw new FaultException<AddBagageFault>(new AddBagageFault()
                {
                    Message = excp.Message
                });
            }
            
        }

        public BagageDefinition GetBagageByCodeIata(string codeItata)
        {
            List<BagageDefinition> list = MyAirport.Pim.Model.Factory.Model.GetBagage(codeItata);
            if (list != null)
            {
                if (list.Count == 1)
                { return list[0]; }
                else
                {
                    throw new FaultException<MultipleBagageFault>(new MultipleBagageFault()
                    {
                        CodeIata = codeItata,
                        Message = "Il existe " + list.Count + " bagages avec le code Iata demandé \"" + codeItata + "\"",
                        ListBagages = list
                    });
                }
            }
            else
                return null;
            //return MyAirport.Pim.Model.Factory.Model.GetBagage(codeItata);
        }

        public BagageDefinition GetBagageById(int idBagage)
        {
            return null;
            //return MyAirport.Pim.Model.Factory.Model.GetBagage(idBagage);
        }

        public RoutageBagage GetInfoRoutage(int idBagage)
        {
            return MyAirport.Pim.Model.Factory.Model.GetInfoRoutage(idBagage);
        }
    }
}
