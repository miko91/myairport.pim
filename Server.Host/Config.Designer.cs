﻿namespace Server.Host
{
    partial class Config
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.testDefault = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.test = new System.Windows.Forms.Button();
            this.useBox = new System.Windows.Forms.CheckBox();
            this.connectionBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.testDefault);
            this.groupBox1.Controls.Add(this.save);
            this.groupBox1.Controls.Add(this.test);
            this.groupBox1.Controls.Add(this.useBox);
            this.groupBox1.Controls.Add(this.connectionBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(376, 129);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base de données";
            // 
            // testDefault
            // 
            this.testDefault.Location = new System.Drawing.Point(125, 95);
            this.testDefault.Name = "testDefault";
            this.testDefault.Size = new System.Drawing.Size(161, 23);
            this.testDefault.TabIndex = 5;
            this.testDefault.Text = "Tester la connexion par défaut";
            this.testDefault.UseVisualStyleBackColor = true;
            this.testDefault.Click += new System.EventHandler(this.testDefault_Click);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(295, 95);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 4;
            this.save.Text = "Enregistrer";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // test
            // 
            this.test.Location = new System.Drawing.Point(7, 95);
            this.test.Name = "test";
            this.test.Size = new System.Drawing.Size(109, 23);
            this.test.TabIndex = 3;
            this.test.Text = "Tester la connexion";
            this.test.UseVisualStyleBackColor = true;
            this.test.Click += new System.EventHandler(this.test_Click);
            // 
            // useBox
            // 
            this.useBox.AutoSize = true;
            this.useBox.Location = new System.Drawing.Point(7, 64);
            this.useBox.Name = "useBox";
            this.useBox.Size = new System.Drawing.Size(136, 17);
            this.useBox.TabIndex = 2;
            this.useBox.Text = "Utiliser cette connexion";
            this.useBox.UseVisualStyleBackColor = true;
            // 
            // connectionBox
            // 
            this.connectionBox.Location = new System.Drawing.Point(7, 37);
            this.connectionBox.Name = "connectionBox";
            this.connectionBox.Size = new System.Drawing.Size(363, 20);
            this.connectionBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Connection string";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(377, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "En cas de problème de configuration, vous pouvez consulter la documentation\r\ndepu" +
    "is le menu Aide -> Documentation";
            // 
            // Config
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 178);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Config";
            this.Text = "MyAirport Server - Configuration";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Button test;
        private System.Windows.Forms.CheckBox useBox;
        private System.Windows.Forms.TextBox connectionBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button testDefault;
        private System.Windows.Forms.Label label2;
    }
}