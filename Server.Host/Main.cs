﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace Server.Host
{
    public partial class Main : Form
    {
        private ServiceHost host = null;
        public Main()
        {
            InitializeComponent();
        }

        private void create_button_Click(object sender, EventArgs e)
        {
            this.host = new ServiceHost(typeof(ServiceBagage.ServiceBagage));
            host.Closed += Host_Closed;
            host.Closing += Host_Closed;
            host.Faulted += Host_Closed;
            host.Opened += Host_Closed;
            host.Opening += Host_Closed;
            

            this.textBox1.Text = host.State.ToString();
            //this.logList.Items.Clear();
            this.log("Création du service");
        }

        private void Host_Closed(object sender, EventArgs e)
        {
            this.textBox1.Text = this.host.State.ToString();
            this.log("Changement d'état : " + this.host.State.ToString());
        }

        private void open_button_Click(object sender, EventArgs e)
        {
            if (this.host != null && this.host.State == CommunicationState.Created)
            {
                this.host.Open();
            }
            else
            {
                this.log("Erreur: Le service doit être créé avant d'être ouvert.");
            }
            
        }

        private void close_button_Click(object sender, EventArgs e)
        {
            if (this.host != null)
            {
                this.host.Close();
            }
            else
            {
                this.log("Erreur: Le service doit être créé avant d'être fermé.");
            }
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Voulez-vous quitter l'application ?", "MyAirport Serveur", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dr == DialogResult.Yes)
            { System.Windows.Forms.Application.Exit(); }
        }

        private void aProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form about = new AboutBox1();
            about.Show();
        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form config = new Config(this);
            config.Show();
        }

        public void log(string message)
        {
            string time = DateTime.Now.ToString("dd/MM/yyyy H:mm:ss");
            this.logList.Items.Add(time + "\t" + message);
        }

        private void documentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://myairport.mika-p.fr");
        }

        public void saveConfiguration()
        {
            ServiceBagage.ServiceBagage.setConnetion();
        }
    }
}
