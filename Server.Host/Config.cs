﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace Server.Host
{
    public partial class Config : Form
    {
        private Main controller;

        public Config(Main controller)
        {
            InitializeComponent();
            this.controller         = controller;
            this.connectionBox.Text = Properties.Settings.Default.Test;
            this.useBox.Checked     = Properties.Settings.Default.UseConnection;
        }

        private void test_Click(object sender, EventArgs e)
        {
            string connection = this.connectionBox.Text;
            connection.Trim();
            if (!connection.Equals("") && !connection.Equals(string.Empty))
            {
                try
                {
                    bool test = this.testConnection(connection);
                    if (test)
                    {
                        MessageBox.Show("Connexion réussie", "Test de la connexion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception exp)
                {
                    MessageBox.Show("Echec de connexion : " + exp.Message, "Test de la connexion", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("La connection string doit être renseignée", "Test de la connexion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool testConnection(string connection)
        {
            ConnectionStringSettings cs = new ConnectionStringSettings("user", connection, "System.Data.SqlClient");
            try
            {
                SqlConnection connect = new SqlConnection(cs.ConnectionString);
                connect.Open();
                return true;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void save_Click(object sender, EventArgs e)
        {
            string connection = this.connectionBox.Text;
            if (!connection.Equals("") && !connection.Equals(string.Empty))
            {
                DialogResult dr = MessageBox.Show("Voulez-vous enregistrer la configuration ?", "Enregistrer la configuration", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (dr == DialogResult.Yes)
                {
                    try
                    {
                        bool test = this.testConnection(connection);
                        if (test)
                        {
                            Properties.Settings.Default.Test = connection;
                            Properties.Settings.Default.UseConnection = this.useBox.Checked;
                            Properties.Settings.Default.Save();
                            this.controller.saveConfiguration();
                            this.controller.log("Configuration enregistrée");
                            Close();
                        }
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show("Impossible d'enregistrer : " + exp.Message, "Enregistrer la configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }       
            }
            else
            {
                MessageBox.Show("La connection string doit être renseignée", "Enregistrer la configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void testDefault_Click(object sender, EventArgs e)
        {
            string connection = ConfigurationManager.ConnectionStrings["MyAirport.Pim.Settings.DbConnect"].ConnectionString;
            try
            {
                bool test = this.testConnection(connection);
                if (test)
                {
                    MessageBox.Show("Connexion réussie", "Test de la connexion par défaut", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Echec de connexion : " + exp.Message, "Test de la connexion par défaut", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
